package com.ciandt.book.seeker.common.di

import androidx.room.Room
import com.ciandt.book.seeker.BuildConfig
import com.ciandt.book.seeker.common.base.ThreadContextProvider
import com.ciandt.book.seeker.common.data.AppDataBase
import com.ciandt.book.seeker.common.data.AppDataBase.Companion.NAME
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val appModule = module {
    factory { ThreadContextProvider()}
}

val remoteModule = module {
    single { Gson() }
    single { GsonConverterFactory.create(get<Gson>())}
    single { HttpLoggingInterceptor() }
    single { OkHttpClient.Builder().addInterceptor(get<HttpLoggingInterceptor>().setLevel(HttpLoggingInterceptor.Level.BODY)).build()}
    single {
        Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(get<GsonConverterFactory>())
            .client(get())
            .build()
    }
}

val localModule = module {
        single {
            Room.databaseBuilder(androidApplication(), AppDataBase::class.java, NAME)
                .fallbackToDestructiveMigration()
                .build()
        }
}