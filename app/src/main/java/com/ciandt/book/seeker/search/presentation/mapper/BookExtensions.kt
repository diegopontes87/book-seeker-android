package com.ciandt.book.seeker.search.presentation.mapper

import com.ciandt.book.seeker.search.presentation.entity.BookPresenter
import com.ciandt.book.seeker.search.repository.localrepository.entity.BookLocal
import com.ciandt.book.seeker.search.repository.remoterepository.entity.BookRemote

fun BookLocal.mapToPresenter() = BookPresenter(
    artistId,
    artistIds,
    artistName,
    artistViewUrl,
    artworkUrl100,
    artworkUrl60,
    currency,
    description,
    fileSizeBytes,
    formattedPrice,
    genreIds,
    genres,
    kind,
    price,
    releaseDate,
    trackCensoredName,
    trackId,
    trackName,
    trackViewUrl
)

fun BookPresenter.mapToLocal() = BookLocal(
    artistId,
    artistIds,
    artistName,
    artistViewUrl,
    artworkUrl100,
    artworkUrl60,
    currency,
    description,
    fileSizeBytes,
    formattedPrice,
    genreIds,
    genres,
    kind,
    price,
    releaseDate,
    trackCensoredName,
    trackId,
    trackName,
    trackViewUrl
)

fun BookRemote.mapToLocal(searchedSentence: String) = BookLocal(
    artistId,
    artistIds,
    artistName,
    artistViewUrl,
    artworkUrl100,
    artworkUrl60,
    currency,
    description,
    fileSizeBytes,
    formattedPrice,
    genreIds,
    genres,
    kind,
    price,
    releaseDate,
    trackCensoredName,
    trackId,
    trackName,
    trackViewUrl,
    searchedSentence = searchedSentence
)