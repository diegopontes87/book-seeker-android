package com.ciandt.book.seeker.search.presentation.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ciandt.book.seeker.R
import com.ciandt.book.seeker.common.base.BaseFragment
import com.ciandt.book.seeker.common.base.ScreenState
import com.ciandt.book.seeker.common.extensions.observe
import com.ciandt.book.seeker.search.presentation.ui.SearchFragment.Companion.SEARCHED_SENTENCE
import com.ciandt.book.seeker.search.presentation.viewmodel.BookListViewModel
import kotlinx.android.synthetic.main.fragment_book_list.*
import org.koin.android.ext.android.inject

class BookListFragment : BaseFragment() {

    private val bookListViewModel: BookListViewModel by inject()
    var searchedSentence: String? = null
    var bookAdapter: BookListAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_book_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startObservers()
        findArguments()
        getBookList()
    }

    private fun findArguments() {
        searchedSentence = arguments?.getString(SEARCHED_SENTENCE)
    }

    private fun getBookList() {
        searchedSentence?.let { bookListViewModel.getLocalBookList(it) }
    }

    private fun startObservers() {
        observe(bookListViewModel.screenStateLiveData) { state ->
            when (state) {
                is ScreenState.Success -> {
                    bookAdapter = BookListAdapter(navController)
                    book_list_screen_recyclerview.adapter = bookAdapter
                    state.data?.let { bookList ->
                        bookAdapter?.bookList = bookList
                    }
                }
            }
        }
    }

    override fun onDetach() {
        navController.popBackStack(R.id.searchFragment, true)
        super.onDetach()
    }
}
