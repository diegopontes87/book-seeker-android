package com.ciandt.book.seeker.search.repository.localrepository.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Books")
data class BookLocal(
    @PrimaryKey val artistId: Int = 0,
    val artistIds: List<Int>? = null,
    val artistName: String? = null,
    val artistViewUrl: String? = null,
    val artworkUrl100: String? = null,
    val artworkUrl60: String? = null,
    val currency: String? = null,
    val description: String? = null,
    val fileSizeBytes: Int? = null,
    val formattedPrice: String? = null,
    val genreIds: List<String>? = null,
    val genres: List<String>? = null,
    val kind: String? = null,
    val price: Double? = null,
    val releaseDate: String? = null,
    val trackCensoredName: String? = null,
    val trackId: Int? = null,
    val trackName: String? = null,
    val trackViewUrl: String? = null,
    var searchedSentence: String = ""
)