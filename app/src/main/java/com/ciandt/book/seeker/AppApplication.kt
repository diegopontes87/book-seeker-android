package com.ciandt.book.seeker

import android.app.Application
import com.ciandt.book.seeker.common.di.appModule
import com.ciandt.book.seeker.common.di.localModule
import com.ciandt.book.seeker.common.di.remoteModule
import com.ciandt.book.seeker.search.di.searchModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class AppApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@AppApplication)
            modules(
                listOf(
                    appModule,
                    localModule,
                    remoteModule,
                    searchModule
                )
            )
        }
    }
}