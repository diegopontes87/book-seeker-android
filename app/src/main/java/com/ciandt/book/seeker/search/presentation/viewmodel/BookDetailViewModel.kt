package com.ciandt.book.seeker.search.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import com.ciandt.book.seeker.common.base.BaseViewModel
import com.ciandt.book.seeker.common.base.ThreadContextProvider
import com.ciandt.book.seeker.search.presentation.entity.BookPresenter
import com.ciandt.book.seeker.search.presentation.mapper.mapToPresenter
import com.ciandt.book.seeker.search.repository.localrepository.dao.BookDao

class BookDetailViewModel(
    threadContextProvider: ThreadContextProvider,
    private val bookDao: BookDao
) : BaseViewModel(threadContextProvider) {

    var book: BookPresenter = BookPresenter()

    var bookLiveData = MutableLiveData<BookPresenter>()

    fun getBook(bookId : Int){
        launchUI{
            launchIO{
                book = bookDao.getBookById(bookId).mapToPresenter()
                launchUI{
                    bookLiveData.value = book
                }
            }

        }
    }
}