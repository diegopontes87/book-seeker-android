package com.ciandt.book.seeker.search.presentation.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.ciandt.book.seeker.R
import com.ciandt.book.seeker.common.base.BaseFragment.Companion.BOOK_ID
import com.ciandt.book.seeker.common.extensions.observe
import com.ciandt.book.seeker.search.presentation.entity.BookPresenter
import com.ciandt.book.seeker.search.presentation.viewmodel.BookDetailViewModel
import kotlinx.android.synthetic.main.fragment_book_detail.*
import org.koin.android.ext.android.inject


/**
 * A simple [Fragment] subclass.
 */
class BookDetailFragment : Fragment() {

    var bookId: Int? = null
    val bookDetailViewModel: BookDetailViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_book_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeLiveData()
        findArguments()
        getBookById()
    }

    private fun findArguments() {
        bookId = arguments?.getInt(BOOK_ID)
    }

    private fun getBookById() {
        bookId?.let { bookDetailViewModel.getBook(it) }
    }

    private fun observeLiveData() {
        observe(bookDetailViewModel.bookLiveData) { book ->
            book?.let {
                setupView(book)
            }
        }
    }

    private fun setupView(book: BookPresenter) {
        Glide.with(this)
            .load(book.artworkUrl100?.replace("100x100bb", "300x300bb"))
            .into(book_detail_screen_imageview)
        book_detail_screen_title.text = book.artistName
        book_detail_screen_description.text = book.description?.let { removeHtml(it) }
        book_detail_screen_price.text = book.formattedPrice
        book_detail_screen_author.text = book.artistName
    }

    private fun removeHtml(html: String): String {
        var html = html
        html = html.replace("<(.*?)\\>".toRegex(), " ")
        html = html.replace("<(.*?)\\\n".toRegex(), " ")
        html = html.replaceFirst("(.*?)\\>".toRegex(), " ")
        html = html.replace("&nbsp;".toRegex(), " ")
        html = html.replace("&amp;".toRegex(), " ")
        return html
    }
}
