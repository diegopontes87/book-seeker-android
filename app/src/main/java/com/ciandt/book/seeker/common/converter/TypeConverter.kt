package com.ciandt.book.seeker.common.converter

interface TypeConverter<Input, Output> {

    fun from(input: Input): Output

    fun to(output: Output): Input

}