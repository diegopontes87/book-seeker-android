package com.ciandt.book.seeker.search.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.ciandt.book.seeker.common.base.BaseViewModel
import com.ciandt.book.seeker.common.base.ScreenState
import com.ciandt.book.seeker.common.base.ThreadContextProvider
import com.ciandt.book.seeker.search.presentation.entity.BookPresenter
import com.ciandt.book.seeker.search.presentation.mapper.mapToPresenter
import com.ciandt.book.seeker.search.repository.localrepository.dao.BookDao
import com.ciandt.book.seeker.search.repository.localrepository.entity.BookLocal

class BookListViewModel(
    threadContextProvider: ThreadContextProvider,
    private val bookDao: BookDao
) : BaseViewModel(threadContextProvider) {

    var screenStateLiveData = MutableLiveData<ScreenState<List<BookPresenter>>>()
    var bookList: List<BookPresenter>? = emptyList()
    var localBookList: List<BookLocal>? = emptyList()

    fun getLocalBookList(searchedSentence: String) {
        try {
            launchUI {
                screenStateLiveData.value = ScreenState.Loading
                withContextIO {
                    localBookList = bookDao.getBookListByName(searchedSentence)
                    if (!localBookList.isNullOrEmpty()) {
                        bookList = localBookList?.map { book -> book.mapToPresenter() }
                    } else {
                        launchUI {
                            ScreenState.Failure
                        }
                    }
                }
                screenStateLiveData.value = ScreenState.Success(bookList)
            }
        } catch (e: Exception) {
            Log.d("ROOM_BOOK_LIST", e.toString())
        }
    }
}