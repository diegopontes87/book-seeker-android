package com.ciandt.book.seeker.common.converter

import com.google.gson.JsonArray

class ListStringTypeConverter : TypeConverter<List<String>, String> {

    @androidx.room.TypeConverter
    override fun from(input: List<String>): String {
        val jsonArray = JsonArray()
        input.forEach { jsonArray.add(it) }
        return jsonArray.toString()
    }

    @androidx.room.TypeConverter
    override fun to(output: String): List<String> {
        return arrayListOf()
    }
}