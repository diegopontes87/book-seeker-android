package com.ciandt.book.seeker.search.repository.localrepository.dao

import androidx.room.*
import com.ciandt.book.seeker.search.repository.localrepository.entity.BookLocal

@Dao
interface BookDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveBookInfo(vararg books: BookLocal)

    @Delete
    fun delete(vararg book: BookLocal)

    @Query("SELECT * FROM Books WHERE searchedSentence LIKE '%' || :searchedSentence || '%'")
    fun getBookListByName(searchedSentence: String): List<BookLocal>?

    @Query("SELECT * FROM Books WHERE artistId = :id")
    fun getBookById(id: Int): BookLocal

    @Query("SELECT DISTINCT searchedSentence FROM Books WHERE searchedSentence NOT NULL")
    fun getSearchedWords(): List<String>


}

