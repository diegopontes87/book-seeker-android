package com.ciandt.book.seeker.common.base

sealed class ScreenState <out T>  {
    object Loading: ScreenState<Nothing>()
    data class Success<T>(val data: T? = null): ScreenState<T>()
    object Failure: ScreenState<Nothing>()
}