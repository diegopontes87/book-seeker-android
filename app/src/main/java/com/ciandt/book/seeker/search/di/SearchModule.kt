package com.ciandt.book.seeker.search.di

import com.ciandt.book.seeker.common.data.AppDataBase
import com.ciandt.book.seeker.search.presentation.viewmodel.BookDetailViewModel
import com.ciandt.book.seeker.search.presentation.viewmodel.BookListViewModel
import com.ciandt.book.seeker.search.presentation.viewmodel.SearchViewModel
import com.ciandt.book.seeker.search.repository.remoterepository.service.ItunesService
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val searchModule = module {
    single { get<Retrofit>().create(ItunesService::class.java) }
    single { get<AppDataBase>().bookDAO() }
    viewModel {
        SearchViewModel(
            threadContextProvider = get(),
            itunesService = get(),
            bookDao = get()
        )
    }

    viewModel {
        BookListViewModel(
            threadContextProvider = get(),
            bookDao = get()
        )
    }

    viewModel {
        BookDetailViewModel(
            threadContextProvider = get(),
            bookDao = get()
        )
    }
}

