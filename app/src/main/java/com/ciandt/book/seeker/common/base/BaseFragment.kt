package com.ciandt.book.seeker.common.base

import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController

open class BaseFragment : Fragment() {
    val navController: NavController by lazy {findNavController()}

    companion object{
        const val BOOK_ID = "BOOK_ID"
    }
}