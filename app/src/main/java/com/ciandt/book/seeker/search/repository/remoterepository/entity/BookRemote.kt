package com.ciandt.book.seeker.search.repository.remoterepository.entity

data class BookRemote(
    val artistId: Int,
    val artistIds: List<Int>,
    val artistName: String,
    val artistViewUrl: String,
    val artworkUrl100: String,
    val artworkUrl60: String,
    val currency: String,
    val description: String,
    val fileSizeBytes: Int,
    val formattedPrice: String,
    val genreIds: List<String>,
    val genres: List<String>,
    val kind: String,
    val price: Double,
    val releaseDate: String,
    val trackCensoredName: String,
    val trackId: Int,
    val trackName: String,
    val trackViewUrl: String
)