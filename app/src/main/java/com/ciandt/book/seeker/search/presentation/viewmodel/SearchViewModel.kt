package com.ciandt.book.seeker.search.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.ciandt.book.seeker.common.base.BaseViewModel
import com.ciandt.book.seeker.common.base.ScreenState
import com.ciandt.book.seeker.common.base.ThreadContextProvider
import com.ciandt.book.seeker.search.presentation.mapper.mapToLocal
import com.ciandt.book.seeker.search.repository.localrepository.dao.BookDao
import com.ciandt.book.seeker.search.repository.remoterepository.entity.ItunesRemoteBooksResponse
import com.ciandt.book.seeker.search.repository.remoterepository.service.ItunesService

class SearchViewModel(
    threadContextProvider: ThreadContextProvider,
    private val itunesService: ItunesService,
    private val bookDao: BookDao
) : BaseViewModel(threadContextProvider) {

    private var remoteResponse: ItunesRemoteBooksResponse? = null
    var screenStateLiveData = MutableLiveData<ScreenState<String>>()
    var searchedSentencesLiveData = MutableLiveData<List<String>>()
    private var searchedBookSentences: List<String>? = emptyList()

    suspend fun getBookList(seachedSentence: String) {
        try {
            launchUI {
                screenStateLiveData.value = ScreenState.Loading
                withContextIO {
                    remoteResponse = itunesService.getBookList(seachedSentence, IBOOK_TYPE)
                    if (!remoteResponse?.results.isNullOrEmpty()) {
                        saveBookListLocal(remoteResponse, seachedSentence)
                    } else {
                        launchUI {
                            screenStateLiveData.value = ScreenState.Failure
                        }
                    }
                }
            }
        } catch (e: Exception) {
            Log.d("RETROFIT_E", e.toString())
        }
    }

    private fun saveBookListLocal(
        remoteResponse: ItunesRemoteBooksResponse?,
        searchedSentence: String
    ) {
        val localBookList = remoteResponse?.results?.map { it ->
            it.mapToLocal(searchedSentence)
        }
        try {
            localBookList?.let { localList ->
                bookDao.saveBookInfo(*localList.toTypedArray())
                launchUI {
                    screenStateLiveData.value = ScreenState.Success(searchedSentence)
                }
            }
        } catch (e: Exception) {
            Log.d("ROOM_E", e.toString())
        }
    }

    fun getTrendingList() {
        launchIO {
            searchedBookSentences = bookDao.getSearchedWords()
            launchUI {
                searchedSentencesLiveData.value = searchedBookSentences
            }
        }
    }

    companion object {
        const val IBOOK_TYPE = "ibook"
    }
}