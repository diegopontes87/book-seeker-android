package com.ciandt.book.seeker.common.base

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

abstract class BaseViewModel(val threadContextProvider: ThreadContextProvider): ViewModel() {

    private val job: Job by lazy { Job() }

    protected val coroutineScope: CoroutineScope by lazy { CoroutineScope(threadContextProvider.io + job) }

    protected inline fun launchIO(crossinline execution: suspend CoroutineScope.() -> Unit) {
        coroutineScope.launch(threadContextProvider.io) { execution() }
    }

    protected inline fun launchUI(crossinline execution: suspend CoroutineScope.() -> Unit) {
        coroutineScope.launch(threadContextProvider.ui) { execution() }
    }

    protected suspend inline fun <reified T> withContextIO(crossinline execution: suspend CoroutineScope.() -> T): T {
        return withContext(threadContextProvider.io) { execution() }
    }

    protected suspend inline fun <reified T> withContextDefault(crossinline execution: suspend CoroutineScope.() -> T): T {
        return withContext(threadContextProvider.default) { execution() }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}