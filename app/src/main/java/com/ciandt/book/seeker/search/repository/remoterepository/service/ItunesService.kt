package com.ciandt.book.seeker.search.repository.remoterepository.service

import com.ciandt.book.seeker.search.repository.remoterepository.entity.ItunesRemoteBooksResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ItunesService {

    @GET("search?")
    suspend fun getBookList(@Query("term")bookName: String, @Query("entity") type: String): ItunesRemoteBooksResponse
}