package com.ciandt.book.seeker.search.presentation.ui

import android.content.Context
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ciandt.book.seeker.R
import com.ciandt.book.seeker.common.base.BaseFragment.Companion.BOOK_ID
import com.ciandt.book.seeker.common.extensions.inflateInto
import com.ciandt.book.seeker.search.presentation.entity.BookPresenter
import kotlinx.android.synthetic.main.adapter_book_list.view.*

class BookListViewHolder(parent: ViewGroup, private val findNavController: NavController) :
    RecyclerView.ViewHolder(parent.inflateInto(R.layout.adapter_book_list)) {

    private val context: Context
        get() = itemView.context

    fun setupView(book: BookPresenter){
        itemView.adapter_book_name.text = book.trackName
        itemView.adapter_authors_name.text = book.artistName
        itemView.adapter_price.text = book.formattedPrice
        Glide.with(context)
            .load(book.artworkUrl100?.replace("100x100bb", "300x300bb"))
            .into(itemView.adapter_image_view)
        itemView.setOnClickListener{
            val bookId = book.artistId
            val bundle = bundleOf(BOOK_ID to bookId)
            findNavController.navigate(R.id.action_bookListFragment_to_bookDetailFragment, bundle)
        }
    }
}

