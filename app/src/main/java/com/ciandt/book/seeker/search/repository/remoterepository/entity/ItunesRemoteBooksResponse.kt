package com.ciandt.book.seeker.search.repository.remoterepository.entity

data class ItunesRemoteBooksResponse(
    val resultCount: Int,
    val results: List<BookRemote>
)