package com.ciandt.book.seeker.search.presentation.ui

import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.ciandt.book.seeker.search.presentation.entity.BookPresenter

class BookListAdapter(private val navController: NavController) : RecyclerView.Adapter<BookListViewHolder>() {

    var bookList: List<BookPresenter> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookListViewHolder {
        return BookListViewHolder(parent, navController)
    }

    override fun getItemCount(): Int {
        return bookList.size
    }

    override fun onBindViewHolder(holder: BookListViewHolder, position: Int) {
        holder.setupView(bookList[position])
    }

}