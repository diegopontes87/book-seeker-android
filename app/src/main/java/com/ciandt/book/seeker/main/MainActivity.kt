package com.ciandt.book.seeker.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import com.ciandt.book.seeker.R
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupView()
        setupNavigation()
        setBottomNavigationListener()
    }

    private fun setupNavigation() {
        navController = Navigation.findNavController(
            this,
            R.id.nav_host_fragment
        )
        bottomNavigation.setupWithNavController(
            Navigation.findNavController(
                this,
                R.id.nav_host_fragment
            )
        )
    }

    private fun setBottomNavigationListener() {
        bottomNavigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.readingNowFragment -> {
                    navController.navigate(R.id.readingNowFragment)
                    setSupportActionBarTitle(getString(R.string.navigation_reading_now))
                    true
                }
                R.id.libraryFragment -> {
                    navController.navigate(R.id.libraryFragment)
                    setSupportActionBarTitle(getString(R.string.navigation_library))
                    true
                }
                R.id.bookStoreFragment -> {
                    navController.navigate(R.id.bookStoreFragment)
                    setSupportActionBarTitle(getString(R.string.navigation_book_store))
                    true
                }
                R.id.searchFragment -> {
                    navController.navigate(R.id.searchFragment)
                    setSupportActionBarTitle(getString(R.string.navigation_search))
                    true
                }
                else -> {
                    false
                }
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    private fun setupView() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.navigation_reading_now)
        supportActionBar?.setShowHideAnimationEnabled(true)
    }

    private fun setSupportActionBarTitle(title: String) {
        supportActionBar?.title = title
    }
}
