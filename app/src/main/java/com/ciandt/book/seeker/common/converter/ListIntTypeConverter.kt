package com.ciandt.book.seeker.common.converter

import com.google.gson.JsonArray

class ListIntTypeConverter : TypeConverter<List<Int>, String> {

    @androidx.room.TypeConverter
    override fun from(input: List<Int>): String {
        val jsonArray = JsonArray()
        input.forEach { jsonArray.add(it) }
        return jsonArray.toString()
    }

    @androidx.room.TypeConverter
    override fun to(output: String): List<Int> {
        return arrayListOf()
    }
}