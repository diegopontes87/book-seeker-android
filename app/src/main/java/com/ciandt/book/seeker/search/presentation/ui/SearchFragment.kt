package com.ciandt.book.seeker.search.presentation.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.SearchView
import android.widget.Toast
import androidx.core.os.bundleOf
import com.ciandt.book.seeker.R
import com.ciandt.book.seeker.common.base.BaseFragment
import com.ciandt.book.seeker.common.base.ScreenState
import com.ciandt.book.seeker.common.extensions.observe
import com.ciandt.book.seeker.search.presentation.viewmodel.SearchViewModel
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.coroutines.runBlocking
import org.koin.android.ext.android.inject

class SearchFragment : BaseFragment() {

    private val searchViewModel: SearchViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setSearchViewOnClick()
        startObservers()
        setListView()
    }

    private fun setSearchViewOnClick() {
        search_screen_searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let {
                    searchSentence(query)
                    search_screen_searchView.clearFocus()
                }
                return true
            }
            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }

        })
    }

    private fun searchSentence(searchedSentence: String) {
        runBlocking {
            searchViewModel.getBookList(searchedSentence)
        }
    }

    private fun startObservers() {
        observe(searchViewModel.screenStateLiveData) { state ->
            when (state) {
                is ScreenState.Loading -> progressBar.visibility = View.VISIBLE
                is ScreenState.Success -> {
                    val bundle = bundleOf(SEARCHED_SENTENCE to state.data)
                    navController.navigate(R.id.action_searchFragment_to_bookListFragment, bundle)
                    progressBar.visibility = View.GONE
                }
                is ScreenState.Failure -> {
                    progressBar.visibility = View.GONE
                    Toast.makeText(
                        context,
                        R.string.nothing_found,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }

        observe(searchViewModel.searchedSentencesLiveData){list ->
            if(!list.isNullOrEmpty()){
                val lastBooksAdapter =
                    context?.let { ArrayAdapter<String>(it,  android.R.layout.simple_list_item_1, list) }

                lastBooksAdapter?.let { adapter ->
                    listView.adapter = adapter
                }
            }
        }
    }

    private fun setListView(){
       searchViewModel.getTrendingList()
    }

    companion object{
        const val SEARCHED_SENTENCE = "SEARCHED_SENTENCE"
    }
}
