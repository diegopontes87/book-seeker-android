package com.ciandt.book.seeker.common.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.ciandt.book.seeker.common.converter.ListIntTypeConverter
import com.ciandt.book.seeker.common.converter.ListStringTypeConverter
import com.ciandt.book.seeker.search.repository.localrepository.dao.BookDao
import com.ciandt.book.seeker.search.repository.localrepository.entity.BookLocal

@Database(entities = [BookLocal::class], version = 1, exportSchema = false)
@TypeConverters(ListIntTypeConverter::class, ListStringTypeConverter::class)
abstract class AppDataBase: RoomDatabase() {

    abstract fun bookDAO() : BookDao

    companion object{
        const val NAME = "bookseeker_db"
        const val PROPERTY_KEY_IS_TEST = "is_test"
    }
}